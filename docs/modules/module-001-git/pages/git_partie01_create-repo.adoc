= Créer un dépôt
ifndef::env-site,env-github,backend-pdf[]
include::_attributes.adoc[]
endif::[]
ifdef::backend-pdf[]
:moduledir: {global_path}docs/modules/module-001-git
:imagesdir: {moduledir}/assets/images/
endif::[]
// Settings
:idprefix:
:idseparator: -

La première partie de cette série va se concentrer sur la création d’un dépôt Git. Afin de réfléter l’usage actuel qui allie le plus souvent un dépôt Git local et un dépôt distant, nous nous appuierons sur https://about.gitlab.com/[Gitlab.com], excellente forge logicielle en ligne basée sur le logiciel libre https://gitlab.com/gitlab-org/gitlab-ce[Gitlab]. indexterm:[git]

[NOTE]
====
Vous pouvez également utiliser l'instance gitlab de https://framagit.org/[framagit] qui héberge notamment les sources de Framatux.
====

== Qu’est-ce qu’un dépôt de sources ? 

indexterm:[dépôt]

Le dépôt de sources est très concrètement un répertoire sur votre système, contenant lu-même un répertoire caché nommé `.git` à sa racine. Vous y stockerez votre code source et il enregistrera les modifications apportées au code dont il a la charge.

Pour prendre directement de bonnes habitudes, nous allons créer un nouveau projet à partir de notre compte Gitlab.com. Un projet Gitlab offre un dépôt distant et une suite d’outils autour. Dans un premier temps nous ne nous intéresserons qu’au dépôt. Le but est, dans un premier temps, de montrer le lien entre le dépôt distant et le dépôt local.

== Pourquoi un dépôt distant ?

Tout simplement dans un premier temps pour sauver votre code sur un autre ordinateur que le vôtre, et dans un second temps de permettre le travail collaboratif.

== Créer un projet sur Gitlab.com

Nous commençons par créer un compte sur Gitlab.com.

image::gitlab-create-account.png[scaledwidth="100%"]

Une fois connecté, nous créons maintenant un nouveau projet.

image::gitlab-create-project.png[scaledwidth="100%"]

Nous lui donnons également un nom. Ce projet n’ayant pas (encore) comme but de devenir public, nous restons en dépôt privé, ce qui signifie que l’accès au dépôt sera restreint par un identifiant et un mot de passe.

image::gitlab-give-name-visibility.png[scaledwidth="100%"]

Nous en avons fini avec Gitlab.com. De retour à Git et à la ligne de commande.

== Cloner un dépôt distant

Sur notre poste, nous allons commencer par cloner le dépôt distant depuis Gitlab.com vers notre poste local : indexterm:[git clone]

[source,bash]
----
$ git clone https://gitlab.com/chaica/toto.git
Cloning into 'toto'...
warning: You appear to have cloned an empty repository.
$ cd toto
$ ls -a
.  ..  .git
----

Comment se souvenir d’où vient ce code ? La commmande `git remote` va nous permettre de voir le lien entre notre dépôt local et le dépôt distant de Gitlab.com :

[source,bash]
----
$ git remote -v
origin https://gitlab.com/chaica/toto (fetch)
origin https://gitlab.com/chaica/toto (push)
----

Nous détaillerons plus tard, l’important est de voir qu’il y a bien un lien entre le dépôt local et le dépôt distant.

== S’identifier

Le dépôt Git dans lequel vous travaillez va bientôt chercher à identifier la personne qui procède à des modifications. Pour cela nous allons définir notre identité au niveau local – c’est-à-dire de ce dépôt – avec les commandes suivantes : indexterm:[git config]

[source,bash]
----
$ git config --local user.name "Carl Chenet"
$ git config --local user.email "chaica@ohmytux.com"
----

Git créé en fait ici un fichier `.git/config` contenant les informations fournies.

Vous pouvez également définir votre identité au niveau global, pour ré-utiliser cette configuration pour tous les dépôts que vous créerez avec cet utilisateur du système :

[source,bash]
----
$ git config --global user.name "Carl Chenet"
$ git config --global user.email "chaica@ohmytux.com"
----

Git crée en fait ici un fichier `~/.gitconfig` contenant les informations fournies.

== Saisir le mot de passe le moins possible

Lorsque nous avons cloné notre dépôt plus haut dans l’article, vous avez dû saisir un mot de passe. J’imagine que vous avez choisi un mot de passe à 24 caractères pour protéger votre compte Gitlab.com, qui est donc également le mot de passe de votre dépôt distant. Dans les prochains articles, nous allons beaucoup interagir avec le dépôt distant et il serait assez pénible d’avoir à le saisir régulièrement. Pour remédier à cela nous allons immédiatement passer la commande suivante :

[source,bash]
----
$ git config --local credential.helper cache
----

Le dernier argument indique le type de stockage du mot de passe. Ici il s’agit uniquement d’un cache d’une durée de vie égale à 15 minutes. Si vous souhaitez stocker définitivement votre mot de passe, vous pouvez utiliser la commande suivante :

[source,bash]
----
$ git config credential.helper store
----

Vos mots de passe seront stockés par défaut dans ~/.git-credentials. Attention, les mots de passe sont sauvés en clair dans ce fichier.

== Conclusion

Pour une première approche de Git, nous avons appris à créer un projet sur Gitlab.com, à rapatrier localement le dépôt créé puis à le configurer pour l’utiliser le plus simplement possible. Nous verrons les premières utilisations de ce dépôt dans une prochaine partie.
