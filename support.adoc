////
Les supports de Formatux sont publiés sous licence Creative Commons-BY-SA et sous licence Art Libre.
Vous êtes ainsi libre de copier, de diffuser et de transformer librement les œuvres dans le respect des droits de l’auteur.

    BY : Paternité. Vous devez citer le nom de l’auteur original.
    SA : Partage des Conditions Initiales à l’Identique.

Licence Creative Commons-BY-SA : https://creativecommons.org/licenses/by-sa/3.0/fr/
Licence Art Libre : http://artlibre.org/

Auteurs : Patrick Finet, Xavier Sauvignon, Antoine Le Morvan, Carl Chenet
////
= FORMATUX : DevOPS
Antoine Le Morvan ; Xavier Sauvignon
Version 2.0 du 16 septembre 2019
:chapter-label: Chapitre
:checkedbox: pass:normal[+&#10004;]+]
:description: Support de cours Linux de Formatux - Automatisation - DevOPS
:docinfo:
:doctype: book
:encoding: utf-8
:experimental:
:icons: font
:icon-set: fa
:lang: fr
:numbered:
:pdf-fontsdir: ./theme/fonts/
:pdf-page-size: A4
:pdf-style: formatux-v2
:pdf-stylesdir: ./theme/
:sectnums:
:sectnumlevels: 2
:showtitle:
:source-highlighter: rouge
:source-language: bash
// define title_page to display the titlepage
:title-page:
//:toc!:
:toc: preamble
:toclevels: 2
:toc-title: Table des matières

// global_path is used for global support
:global_path: ./
:numbered!:

include::0000-preface.adoc[]

=== Gestion des versions

.Historique des versions du document
[width="100%",options="header",cols="1,2,4"]
|====================
| Version | Date | Observations
| 1.0 | Avril 2017 | Version initiale.
| 1.1 | Juillet 2017 | Ajout de généralités.
| 1.2 | Aout 2017 | Ajout du cours Jenkins et Rundeck
| 1.3 | Février 2019 | Ajout des cours Ansible Niveau 2, Ansistrano, Asciidoc, Terraform
| 2.0 | Septembre 2019 | Passage à `antora`
|====================

:numbered:

include::support-index.adoc[]

:numbered!:

include::glossary.adoc[]

[index]
== Index
////////////////////////////////////////////////////////////////
The index is normally left completely empty, it's contents being
generated automatically by the DocBook toolchain.
////////////////////////////////////////////////////////////////
